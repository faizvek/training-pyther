package com.basicbleapp.customePackage;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.empatica.empalink.ConnectionNotAllowedException;
import com.empatica.empalink.EmpaDeviceManager;
import com.empatica.empalink.EmpaticaDevice;
import com.empatica.empalink.config.EmpaSensorStatus;
import com.empatica.empalink.config.EmpaSensorType;
import com.empatica.empalink.config.EmpaStatus;
import com.empatica.empalink.delegate.EmpaDataDelegate;
import com.empatica.empalink.delegate.EmpaStatusDelegate;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

public class EmpaticaLinkModule extends ReactContextBaseJavaModule implements EmpaDataDelegate, EmpaStatusDelegate {

    private EmpaDeviceManager deviceManager = null;
    private static final String EMPATICA_API_KEY = "6325f3812a064bde88ca459069cc6d73"; // TODO insert your API Key here
    private static final int REQUEST_PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 1;
    private static String WATCH_STATUS="";
    private static String EMPA_STATUS="";
    private static int ACC_X=0;
    private static int ACC_Y=0;
    private static int[] deviceData ={};
    private static float GSR=0;
    private static float IBI=0;
    private static float TEMPRATURE=0;
    private static float BVP=0;
    private static float BTR_LEVEL=0;

    public EmpaticaLinkModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }
    @ReactMethod
    private void initEmpaticaDeviceManager() {
        // Android 6 (API level 23) now require ACCESS_COARSE_LOCATION permission to use BLE
        if (ContextCompat.checkSelfPermission(this.getReactApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("mylog1","Location permission");
//            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION }, REQUEST_PERMISSION_ACCESS_COARSE_LOCATION);
        } else {
            if (TextUtils.isEmpty(EMPATICA_API_KEY)) {
                new AlertDialog.Builder(this.getReactApplicationContext())
                        .setTitle("Warning")
                        .setMessage("Please insert your API KEY")
                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // without permission exit is the only way

                                System.exit(0);
                            }
                        })
                        .show();
                return;
            }

            // Create a new EmpaDeviceManager. MainActivity is both its data and status delegate.
            deviceManager = new EmpaDeviceManager(getReactApplicationContext(), this, this);

            // Initialize the Device Manager using your API key. You need to have Internet access at this point.
            deviceManager.authenticateWithAPIKey(EMPATICA_API_KEY);
        }
    }

    @Override
    public void didReceiveGSR(float gsr, double timestamp) {
        GSR=gsr;
    }

    @Override
    public void didReceiveBVP(float bvp, double timestamp) {
        BVP=bvp;
    }

    @Override
    public void didReceiveIBI(float ibi, double timestamp) {
        IBI=ibi;
    }

    @Override
    public void didReceiveTemperature(float t, double timestamp) {
        TEMPRATURE=t;
    }

    @Override
    public void didReceiveAcceleration(int x, int y, int z, double timestamp) {
            ACC_X=x;
            ACC_Y=y;
    }

    @Override
    public void didReceiveBatteryLevel(float level, double timestamp) {
        BTR_LEVEL=level;
    }

    @Override
    public void didReceiveTag(double timestamp) {

    }

    @Override
    public void didUpdateStatus(EmpaStatus status) {
        if (status == EmpaStatus.READY) {
            EMPA_STATUS="READY";
            // Start scanning
            deviceManager.startScanning();
            // The device manager has established a connection

        } else if (status == EmpaStatus.CONNECTED) {
            EMPA_STATUS="CONNECTED";
            // The device manager disconnected from a device
        } else if (status == EmpaStatus.DISCONNECTED) {
            EMPA_STATUS="DISCONNECTED";
        }
    }

    @Override
    public void didEstablishConnection() {

    }

    @Override
    public void didUpdateSensorStatus(@EmpaSensorStatus int status, EmpaSensorType type) {
        didUpdateOnWristStatus(status);
    }
    private void updateLabel(final TextView label, final String text) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText(text);
            }
        });
    }
    @Override
    public void didDiscoverDevice(EmpaticaDevice device, String deviceLabel, int rssi, boolean allowed) {
        Log.d("alow log","didDiscoverDevice "+allowed);
        if (allowed) {
            // Stop scanning. The first allowed device will do.
            deviceManager.stopScanning();
            try {
                // Connect to the device
                deviceManager.connectDevice(device);
//                updateLabel(deviceNameLabel, "To: " + deviceName);
                Log.d("mylo2g","Connected ");
            } catch (ConnectionNotAllowedException e) {
                Log.d("mylo4g","Connection Error | "+e.getMessage());
                // This should happen only if you try to connect when allowed == false.
                Toast.makeText(getReactApplicationContext(), "Sorry, you can't connect to this device", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void didRequestEnableBluetooth() {
//        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    @Override
    public void didUpdateOnWristStatus(int status) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                if (status == EmpaSensorStatus.ON_WRIST) {
                    WATCH_STATUS="ON_WRIST";
                }
                else {
                    WATCH_STATUS="NOT ON_WRIST";
                }
            }
        });
    }

    @NonNull
    @Override
    public String getName() {
        return "EmpLink";
    }

    @ReactMethod
    public void getDeviceData(Callback cb){
         deviceData= new int[]{ACC_X, ACC_Y};
        try{
            cb.invoke(null, WATCH_STATUS);
        }catch (Exception e){
            cb.invoke(e.toString(), null);
        }
    }
}
