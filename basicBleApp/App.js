/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {NativeModules} from 'react-native';

const App = () => {
  NativeModules.EmpLink.initEmpaticaDeviceManager();
  NativeModules.EmpLink.getDeviceData((error,xVal,yVal)=>{
    console.log("Error-- "+error );
    console.log("Watch status-- "+xVal );
  });
  return (
    <>
      <Text>Hello</Text>
    </>
  );
};


export default App;
