from django.http import HttpResponse,JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def index(request):
    data = [{
        'name': 'Vitor',
        'location': 'Finland',
        'is_active': True,
        'count': 28
    }]
    if request.method == 'GET':
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        jsonRequest=json.loads(request.body)
        data.append(jsonRequest)
        return JsonResponse(data,safe=False)

    else:
        return HttpResponse("Wrong URL..")

# Create your views here.
